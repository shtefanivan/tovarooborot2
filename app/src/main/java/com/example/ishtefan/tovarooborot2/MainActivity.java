package com.example.ishtefan.tovarooborot2;

/*
2. Создать программу "Товарооборот". На экране есть список товаров
        (5 наименований, больше не берите) и их кол-во. Каждое n количество времени в магазине
        покупают несколько товаров. Список каждый раз должен обновляться.
*/


import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    public static final int QUANTITY_OF_PRODUCT = 10;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    TextView quantity;
    TextView itemForBuy;
    ArrayList<String> product;
    Random randItem;
    int item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        makeProducts();

        quantity = (TextView) findViewById(R.id.am_quantity);
        itemForBuy = (TextView) findViewById(R.id.item_buy_pruduct);
        quantity.setText(Integer.toString(product.size()));
        randItem = new Random();
        new Buyer().execute(product.size());

        recyclerView = (RecyclerView) findViewById(R.id.am_product_list);
        LinearLayoutManager linearLayoutManagerManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManagerManager);
        adapter = new RecyclerViewAdapter(product, this);

        adapter.setListener(new RecyclerViewAdapter.OnRecyclerItemClickListener() {
            @Override
            public void onItemClickListener(String item, int position) {
                Toast.makeText(MainActivity.this, item, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void makeProducts() {

        product = new ArrayList<>();

        for (int i = 0; i < QUANTITY_OF_PRODUCT; i++) {
            product.add(i + " товар");
        }
    }

    class Buyer extends AsyncTask<Integer, String, Long> {

        @Override
        protected Long doInBackground(Integer... params) {

            //do in Thread
            do{
                try {
                    Thread.sleep(1000);
                    item = randItem.nextInt(product.size());
                    product.remove(item);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress();
            }while (product.size()!=0);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Long aVoid) {
            Toast.makeText(MainActivity.this, "Мы распродались. Товаров больше нет", Toast.LENGTH_SHORT).show();
            recyclerView.setAdapter(adapter);
        }

        @Override
        protected void onProgressUpdate(String... values) {

            quantity.setText(Integer.toString(product.size()));
            itemForBuy.setText(Integer.toString(item));

            if (product.size() != 0) {
                recyclerView.setAdapter(adapter);

            }
        }
    }
}
