package com.example.ishtefan.tovarooborot2;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by wenceslaus on 08.10.16.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public interface OnRecyclerItemClickListener {
        void onItemClickListener(String item, int position);
    }

    private List<String> list;
    private Context context;
    private OnRecyclerItemClickListener listener;

    public RecyclerViewAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_view, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.name.setText(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(OnRecyclerItemClickListener listener) {
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;

        ViewHolder(View item) {
            super(item);
            name = (TextView) item.findViewById(R.id.iv_one_product);
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                int position = getAdapterPosition();
                listener.onItemClickListener(list.get(position), position);
            } else {
                throw new RuntimeException("You must init OnRecyclerItemClickListener by calling setListener() method.");
            }
        }
    }
}